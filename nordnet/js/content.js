$(window).bind("load", function() {
    var today_pct_vals = []
    if ($("body").find("table").length) {
        $("tbody tr").each(function() {
            var today_pct = $(this).find("td:nth-child(5)").html();
            var span_today_pct = $(today_pct).find("span:nth-child(2)").html();
            var floatValues =  /[+-]?([0-9]*[.])?[0-9]+/;
            if (span_today_pct.startsWith('-')) {
                var m = floatValues.exec(span_today_pct);
                var val_neg = -Math.abs(m[0])
                today_pct_vals.push(val_neg);
            }
            if (span_today_pct.startsWith('+')) {
                var m = floatValues.exec(span_today_pct);
                var val_pos = Math.abs(m[0])
                today_pct_vals.push(val_pos);
            }
        });
        var total = 0;
        $.each(today_pct_vals,function(){total+=parseFloat(this) || 0;});
        var total_sum = (Number(Number((total).toFixed(2)) / today_pct_vals.length)).toFixed(2)
        var total_pct = total_sum + '%';
        var total_invested = 1000*today_pct_vals.length
        var total_amt = Number((total_invested)*total_sum/100).toFixed(2);
        var total_amt_str = total_amt + '<br>(of ' + total_invested + ')'
        $("tbody").append("<tr style='overflow:visible'><td></td><td></td><td></td><td class='col4' style='text-align: right'></td><td class='col5' style='text-align: right'></td></tr>");
        $('.col4').html(total_pct);
        $('.col5').html(total_amt_str);
    }
});
