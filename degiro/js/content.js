$(window).bind("load", function() {
    var today_pct_vals = []
    setTimeout(function() { 
        if ($("body").find("table").length) {
            //$("tbody tr").each(function() {
            $("tbody tr:not(:first-child)").each(function() {
                var val = $(this).find("td:nth-child(5)").html();
                var span_today_pct = $(val).html();
                var floatValues =  /[+-]?([0-9]*[,])?[0-9]+/;
                var m = floatValues.exec(span_today_pct);
                var val_pct = Number(m[0].replace(',', '.'));
                today_pct_vals.push(val_pct);
            })
            var total = 0;
            $.each(today_pct_vals,function(){total+=parseFloat(this) || 0;});
            var total_sum = (Number(Number((total).toFixed(2)) / today_pct_vals.length)).toFixed(2)
            var total_pct = total_sum + '%';
            var total_invested = 100*today_pct_vals.length
            var total_amt = Number((total_invested)*total_sum/100).toFixed(2);
            var total_amt_str = total_amt + '<br>(of ' + total_invested + ')'
            $("tbody").append("<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td class='col4' style='padding: 0 8px; font-weight: 700; text-align: right;'></td><td class='col5' style='padding: 0 8px; font-weight: 700; text-align: right;'></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
            $('.col4').html(total_amt_str);
            $('.col5').html(total_pct);
        };
    }, 5000 );
});
